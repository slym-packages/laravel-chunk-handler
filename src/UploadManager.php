<?php

namespace CodingSocks\ChunkUploader;

use CodingSocks\ChunkUploader\Driver\BlueimpUploadDriver;
use CodingSocks\ChunkUploader\Driver\DropzoneUploadDriver;
use CodingSocks\ChunkUploader\Driver\FlowJsUploadDriver;
use CodingSocks\ChunkUploader\Driver\MonolithUploadDriver;
use CodingSocks\ChunkUploader\Driver\NgFileUploadDriver;
use CodingSocks\ChunkUploader\Driver\PluploadUploadDriver;
use CodingSocks\ChunkUploader\Driver\ResumableJsUploadDriver;
use CodingSocks\ChunkUploader\Driver\SimpleUploaderJsUploadDriver;
use Illuminate\Support\Manager;

class UploadManager extends Manager
{
    public function createMonolithDriver()
    {
        return new MonolithUploadDriver($this->container['config']['chunk-uploader.monolith']);
    }

    public function createBlueimpDriver()
    {
        /** @var \Illuminate\Support\Manager $identityManager */
        $identityManager = $this->container['chunk-uploader.identity-manager'];

        return new BlueimpUploadDriver($this->container['config']['chunk-uploader.blueimp'], $identityManager->driver());
    }

    public function createDropzoneDriver()
    {
        return new DropzoneUploadDriver($this->container['config']['chunk-uploader.dropzone']);
    }

    public function createFlowJsDriver()
    {
        return new FlowJsUploadDriver($this->container['config']['chunk-uploader.resumable-js'], $this->identityManager()->driver());
    }

    public function createNgFileUploadDriver()
    {
        return new NgFileUploadDriver($this->identityManager()->driver());
    }

    public function createPluploadDriver()
    {
        return new PluploadUploadDriver($this->identityManager()->driver());
    }

    public function createResumableJsDriver()
    {
        return new ResumableJsUploadDriver($this->container['config']['chunk-uploader.resumable-js'], $this->identityManager()->driver());
    }

    public function createSimpleUploaderJsDriver()
    {
        return new SimpleUploaderJsUploadDriver($this->container['config']['chunk-uploader.simple-uploader-js'], $this->identityManager()->driver());
    }

    /**
     * @return \Illuminate\Support\Manager
     */
    protected function identityManager()
    {
        return $this->container['chunk-uploader.identity-manager'];
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->container['config']['chunk-uploader.uploader'];
    }

    /**
     * Set the default mail driver name.
     *
     * @param  string $name
     *
     * @return void
     */
    public function setDefaultDriver($name)
    {
        $this->container['config']['chunk-uploader.uploader'] = $name;
    }
}
