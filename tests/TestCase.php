<?php

namespace CodingSocks\ChunkUploader\Tests;

use CodingSocks\ChunkUploader\ChunkUploaderServiceProvider;
use Illuminate\Http\UploadedFile;
use ReflectionClass;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * {@inheritDoc}
     */
    protected function getPackageProviders($app)
    {
        return [
            ChunkUploaderServiceProvider::class,
        ];
    }

    /**
     * @param $path
     * @param $name
     */
    protected function createFakeLocalFile($path, $name)
    {
        $file = UploadedFile::fake()->create($name);
        $file->storeAs($path, $name, [
            'disk' => 'local',
        ]);
    }

    /**
     * https://github.com/sebastianbergmann/phpunit-mock-objects/issues/257
     *
     * @param $expects
     * @param mixed ...$arguments
     *
     * @return \Closure
     */
    protected function createClosureMock($expects, ...$arguments)
    {
        /** @var \Closure|\PHPUnit\Framework\MockObject\MockObject $callback */
        $callback = $this->getMockBuilder(\stdClass::class)
            ->setMethods(['__invoke'])
            ->getMock();
        $callback->expects($expects)
            ->method('__invoke')
            ->with(...$arguments);

        return function () use ($callback) {
            return $callback(...func_get_args());
        };
    }

    /**
  * Invoke a non-public method.
  *
  * @param  mixed  $object
  * @param  string  $name
  * @param  array  $params
  * @return mixed
  *
  * @throws \ReflectionException
  */
 protected function invokeMethod(&$object, string $name, array $params = [])
 {
     $reflection = new ReflectionClass(get_class($object));
     $method = $reflection->getMethod($name);
     $method->setAccessible(true);

     return $method->invokeArgs($object, $params);
 }

 /**
  * Read a non-public property.
  *
  * @param  mixed  $object
  * @param  string  $name
  * @return mixed
  *
  * @throws \ReflectionException
  */
 protected function readProperty(&$object, string $name)
 {
     $reflection = new ReflectionClass(get_class($object));
     $property = $reflection->getProperty($name);
     $property->setAccessible(true);

     return $property->getValue($object);
 }
}
